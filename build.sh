#! /bin/bash
DIR_NAME=$APP_NAME-$APP_VERSION
FILE_NAME=$APP_NAME-$APP_VERSION.tar.gz

DOWNLOAD_URL=http://ftpmirror.gnu.org/$APP_NAME/$FILE_NAME

# Make a dir for the app, to keep it a bit more organized
mkdir $APP_NAME
cd $APP_NAME

# Fetch the source code
wget $DOWNLOAD_URL

# Extract the package with the source code
tar -xvf $FILE_NAME

cd $DIR_NAME

./configure --help
./configure --prefix=${INSTALL_PATH} --enable-cxx --enable-assert --enable-mpbsd --with-gnu-ld

make -j ${CORES}


make check

make install